// instance => When an object is created from a class, that object is said to be an instance of the class.
// https://www.sitepoint.com/object-oriented-javascript-deep-dive-es6-classes/

// * Book Class: Represents a Book
// Constructor => a special method for creating and initializing an object created within a class.
class Book {
  constructor(title, author, isbn) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

// * UI Class: Handle UI Tasks (methods: display, add and remove books and alerts)
// Static defines a static method for a class. Static methods aren't called on instances of the class. Instead, they're called on the class itself. These are often utility functions, such as functions to create or clone objects.
// more on static - https://medium.com/@yyang0903/static-objects-static-methods-in-es6-1c026dbb8bb1
class UI {
  // displaying the books
  static displayBooks() {
    // this is the local storage (hard coded array)
    // * dummy data
    // const StoredBooks = [
    //   {
    //     title: "Book One",
    //     author: "John Doe",
    //     isbn: "3434434"
    //   },
    //   {
    //     title: "Book Two",
    //     author: "Jane Doe",
    //     isbn: "45545"
    //   }
    // ];
    // const books = StoredBooks;

    // * connected to local storage
    const books = Store.getBooks();

    // forEach => executes a provided function once for each array element.
    books.forEach((book) =>  UI.addBookToList(book))
  }

  // add book
  static addBookToList(book) {
    const list = document.querySelector('#book-list'); // or use "document.getElementById"

    const row = document.createElement('tr'); 
    // add html in there
    row.innerHTML = `
      <td>${book.title}</td>
      <td>${book.author}</td>
      <td>${book.isbn}</td>
      <td><a href="#" class="btn btn-danger btn-sm delete">X</a></td>
    `;

    // append the row to the list
    list.appendChild(row);
  }

  // delete book
  static deleteBook(el) {
    // classList => if a class is included in the class
    if(el.classList.contains('delete')) {
      // want to remove that whole thing (parent (i.e. row) and child)
      // a (child) -> td (parent) -> tr (parent).remove();
      el.parentElement.parentElement.remove();
    }
  }

  // alert to fill out fields (using bootstrap)
  // custom static - building a div from scratch
  // and insert it into the UI
  static showAlert(message, className) {
    // creating a div
    const div = document.createElement('div');
    div.className = `alert alert-${className}`; // shows either success or danger
    div.appendChild(document.createTextNode(message)); // appendChild(textNode) => adding something into the div
    const container = document.querySelector('.container');
    const form = document.querySelector('#book-form');
    container.insertBefore(div, form);

    // Vanish in 3 seconds
    setTimeout(() =>
      document.querySelector('.alert').remove(), 3000
    )
  }

  // clear books
  static clearFields() {
    document.querySelector('#title').value = '';
    document.querySelector('#author').value = '';
    document.querySelector('#isbn').value = '';
  }
}

// * Store Class: Handles Storage (local storage)
// stores "key value pairs"
// have an item call "books" (which is a string version)
// of the entire array of books
// * CANNOT STORE OBJECTS IN LOCAL STORAGE!
// * HAS TO BE A "STRING", SO HAVE TO USE "STRINGIFY"
// * TO PULL IT OUT OF STORAGE => HAVE TO USE "PARSE"
class Store {
  static getBooks() {
    let books;
    // if there is no item of books then want to take
    // that "books" variable and set it to an empty array
    if(localStorage.getItem('books') === null) { // getItem is part of localStorage
      books = [];
    } else {
      // use it as a regular JS array of objects
      books = JSON.parse(localStorage.getItem('books'));
    }

    return books;
  }
  static addBook(book) {
    const books = Store.getBooks();
    books.push(book);
    localStorage.setItem('books', JSON.stringify(books));
  }

  static removeBook(isbn) {
    // isbn = primary key
    const books = Store.getBooks();
    books.forEach((book, index) => {
      if(book.isbn === isbn) {
        books.splice(index, 1);
      }
    });
    localStorage.setItem('books', JSON.stringify(books))
  }
}

// * Event: Display Books
document.addEventListener('DOMContentLoaded', UI.displayBooks);

// * Event: Add a Book
document.querySelector('#book-form').addEventListener('submit', e => {
  // prevent actual submit (default value)
  e.preventDefault();

  // get form values
  const title = document.querySelector('#title').value;
  const author = document.querySelector('#author').value;
  const isbn = document.querySelector('#isbn').value;

  // validate book
  if(title === "" || author === "" || isbn === "") {
    UI.showAlert('Please fill in all fields', 'danger') // see line 50 (coming from bootstrap alert: btn btn-danger)
  } else {
    // instantiate book
    const book = new Book(title, author, isbn);

    // testing to see if book object is shown
    // console.log(book);

    // add book to UI
    UI.addBookToList(book);

    // add book to store (go to "application" to see data)
    Store.addBook(book);

    // alert success message
    UI.showAlert('Book Added', 'success');

    // clear fields
    UI.clearFields();
  }
});

// * Event: Remove a Book
// EVENT PROPAGATION
// (e.g. select something about it like the book 
// list and then target whatever is clicked inside 
// of it )
// * BUBBLING UP AND CAPTURING => https://javascript.info/bubbling-and-capturing

// * Bubbling up: When an event happens on an element, 
// * it first runs the handlers on it, then on its parent, 
// * then all the way up on other ancestors.
// * example is when the inner tag is clicked, 
// * its parent tags are clicked, too

// targeting the book list
document.querySelector('#book-list').addEventListener('click', e => {
  // testing e.target for delete button
  // console.log(e.target);
  // remove book from UI
  UI.deleteBook(e.target);

  Store.removeBook(e.target.parentElement.previousElementSibling.textContent); // a -> td (parent) -> tr (row)

  // alert success message
    UI.showAlert('Book Removed', 'success');
});